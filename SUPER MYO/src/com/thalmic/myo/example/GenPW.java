package com.thalmic.myo.example;


import java.util.Arrays;
import java.util.Scanner;
import java.util.Timer;
import java.util.TimerTask;

import com.thalmic.myo.DeviceListener;
import com.thalmic.myo.Hub;
import com.thalmic.myo.Myo;
import com.thalmic.myo.enums.StreamEmgType;

public class GenPW{
	public static void main(String[] args) {
		try {
			Hub hub = new Hub("com.example.emg-data-sample");
			System.out.println("Attempting to find a Myo...");
			Myo myo = hub.waitForMyo(10000);
			if (myo == null) {
				throw new RuntimeException("Unable to find a Myo!");
			}

			System.out.println("Connected to a Myo armband!");

			System.out.println("When you text: \"GO\" you have 3 seconds to make a secret hand guesture that will be your PW");
			Scanner in = new Scanner(System.in);
			String s = in.next();
			EmgCollectorThing emgCol = null;
			if(s.equals("GO")){
				emgCol = new EmgCollectorThing(hub, myo);
				emgCol.foo();
			}else{
				System.out.println("FAIL");
			}
			for (int[] i : emgCol.getDataPW()) {
				for (int n : i) {
					System.out.print(n + " "); 
				}

				System.out.println();
			}
			System.out.println("PW GENERATED");



		} catch (Exception e) {
			System.err.println("Error: ");
			e.printStackTrace();
			System.exit(1);
		}
	}

}
