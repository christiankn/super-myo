package com.thalmic.myo.example;


import java.util.ArrayList;
import java.util.Scanner;
import java.util.TimerTask;

import com.thalmic.myo.DeviceListener;
import com.thalmic.myo.Hub;
import com.thalmic.myo.Myo;
import com.thalmic.myo.enums.StreamEmgType;

public class EmgCollectorThing{
	private ArrayList<int[]> pwData =new ArrayList<int[]>();
	private Hub hub;
	private Myo myo;
	public EmgCollectorThing(Hub hub, Myo myo){
		this.hub = hub;
		this.myo = myo;
	}
	public void foo(){
		try {
			myo.setStreamEmg(StreamEmgType.STREAM_EMG_ENABLED);
			DeviceListener dataCollector = new EmgDataCollector();
			hub.addListener(dataCollector);
			int i = 0;
			while(i<50) {
				hub.run(1000 / 20);
				int[] temp = strToIntArr(dataCollector.toString());
				pwData.add(temp);
				i++;
			}
		} catch (Exception e) {
			System.err.println("Error: ");
			e.printStackTrace();
			System.exit(1);
		}
	}
	
	public int[] strToIntArr(String arr){
		String[] items = arr.replaceAll("\\[", "").replaceAll("\\]", "").split(",");
		int[] results = new int[items.length];
		for (int i = 0; i < items.length; i++) {
		    try {
		        results[i] = Integer.parseInt(items[i]);
		    } catch (NumberFormatException nfe) {};
		}
		return results;
	}
	
	public ArrayList<int[]> getDataPW(){
		return pwData;	
	}
}


